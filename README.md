# Design tips to update your home

Home decorating isn’t as hard as you’d think – all it takes it some research and a good eye for style.